package testDAOs;

import DAOs.Article;
import DAOs.ArticleDAO;
import DAOs.User;
import DAOs.UserDAO;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.*;


public class TestArticleDAO {
//    @Before
//    public void setUp() {
//
//    }

    @Test
    public void testConnection() {
        try (ArticleDAO artcileDAO = new ArticleDAO()) {

        } catch (SQLException e) {
            fail();
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
        assertEquals(true, true);
    }

    @Test
    public void testGetAllArticles() {
        List<Article> articles;
        try (ArticleDAO articleDAO = new ArticleDAO()) {
            articles = articleDAO.getAllArticles();

            assertTrue(articles.size() > 0);

        } catch (SQLException e) {
            fail();
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
        assertEquals(true, true);
    }

}
