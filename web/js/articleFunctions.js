
                                    //////////Article Modal Functions////////////////
                     //These functions control the behaviour of the modals associated with displaying articles.


function showDelete() {

}


                                ///////////////Modal Functions///////////////////
// These functions operate when the various modals involved in the business of the application are shown or hidden.

/*
When the modal which displays articles is shown, we take the data from the data attributes hidden in the clicked 'Load Article' button
                             and updates the text areas of the modal with this content.
 */

$('#articleModal').on('show.bs.modal', function (event) {
    var modal = $(this);

    var button = $(event.relatedTarget); // Button that triggered the modal
    var articleID = button.data('id');   // Finds the value of the 'data-id' HTML Data Attribute of the button
    var normalTitle=$('#articleModal .modal_title');
    var normalText=$('#articleModal .modal_txt');
    normalTitle.css("display", "block");
    normalText.css("display", "block");


    // Extract info from data-* attributes

    var titleData = button.data('title');// Finds the value of the 'data-title' HTML Data Attribute of the button
    var contentData = button.data('content');  //Finds the value of the 'data-content' HTML Data Attribute of the button
    var username = button.data('username'); // Finds the value of the 'data-username' HTML Data Attribute of the button

    // Updates the modal's content according to the data attribute in the button which was clicked .
    modal.find('.modal_title').text(titleData);
    //use append() instand of text() to display html text
    modal.find('.modal_txt').append(contentData);
    // modal.find('.modal_txt').text(contentData);
    modal.find('#editTextArea').text(contentData);
    modal.find('#editTitle').val(titleData);
    modal.find('.modal_user').text(username);
    modal.find('#modal_id').text(articleID);
    //parse article id to the form, then servlet, chinchien
    modal.find('#editID').val(articleID);

    //Finds the comments in this particular article
    var commentArticleId = document.getElementsByClassName("commentArticleId");
    var comments = document.getElementsByClassName("commentz");

    //loops through all th comments, and changes their display if their id matches the articleID
    for (var i = 0; i < comments.length; i++) {
        var id = commentArticleId[i].innerText;
        var checkComment = comments[i];
        //displays the comments if their id matches the article id
        if (!(id != articleID)) {
            checkComment.style.display = "block";
            // }
        } else {
            checkComment.style.display = "none";
        }
    }




})


/*
This code triggers when the article modal is hidden. It disables display on comments
and nested comments, and wipes the text area where comments are left.
 */

$('#articleModal').on('hide.bs.modal', function () {


    $('.tinyCommentsz').css("display", "none");
    $('.commentz').css("display", "none");

    //

    $('#articleModal .modal_txt').text(" ");
    $('#articleModal #commentTextArea').val(" ");

    $('commentingText').val(" ");
    $('editTextArea').css("display","none");
    $('modal_txt').css("display","block");

})

//wipes nested modal commenting area on hide
$('#nestedModalz').on('hide.bs.modal', function () {
    $('commentingText').val(" ");
})


///////////////Comment Functions///////////////////




//displays all comments. Do we need this?
function displayComment(event) {

    // var button = $(event.relatedTarget);
    $('.commentingText').css("display", "block");
}



//this function displays all the nested comments.
function displayNestedComments(articleID) {
    var nestedComments=document.getElementsByClassName('tinyCommentsz');
    var nestedCommentID=document.getElementsByClassName('tinyID');

    for (var i=0; i<nestedComments.length;i++){

        var commentInQuestion=nestedComments[i];
        var idInQuestion=nestedCommentID[i];
        var toUse=idInQuestion.innerText;

        if (toUse==articleID){
            commentInQuestion.style.display= "block";
        }

    }



}


function idToNested(referenceID) {

    var referenceArea=$('#nestedModalz #referenceID');
    referenceArea.text(referenceID);
}


//for deleting newly left comments and nested comments

$('.ersatzDelete').on('click',function () {
    var buttonParent=$(this).parent();
    buttonParent.css("display","none");
})

 // to deal with attempts to comment on a newly made comment at this stage of our development process ;)
function personalSpace() {

    alert("I don't like to be touched. Try another comment.")
}



//sets a cookie for the servlet,  disables display for particular comment then makes AJAX post request.
function deleteComment(commentId) {


    document.cookie = "edit=delete";

    var button = $(event.relatedTarget); // Button that triggered the modal

    var commentDataButton = button.data('cID');

    $('#' + commentId).css("display", "none");
    $('#' + commentId + "Button").css("display", "none");


    var commentData = {commentID: commentId};
    $.ajax({
        type: 'post',
        url: "/Comment",
        data: commentData,
        dataType: "text",
        success: function (resultData) {


        }
    });

}

//////Editing functions///////

//sets a cookie for the servlet when user wants to edit an article
function editCookie() {
    document.cookie = "edit=true;Secure";
    //This cookie doesn't work for local host testing. please uncomment for deployment
    //  document.cookie = "edit=true;Secure;HttpOnly";
}



//This function changes the modal display when the user wants to edit an article.
function displayEdit() {
    $("#articleModal #editTextArea").summernote({focus: true});
    // var textarea = $("#articleModal #editTextArea");
    var saveButton = $("#articleModal #saveButton");
    var titleArea = $("#articleModal #editTitle");
    // var deleteButton = $("#articleModal .deleteButton");


    var normalTitle=$('#articleModal .modal_title');
    var normalText=$('#articleModal .modal_txt');
    normalTitle.css("display", "none");
    normalText.css("display", "none");
    saveButton.css("display", "block");
    titleArea.css("display", "block");
    // deleteButton.css("display", "block");

    var editButton = $("#editButton");
    editButton.css("display", "none");

}





/*
This function allows for the deletion of articles. After confirming the request, it sets a cookie with the value of
delete then makes an AJAX post request with the ID value of the article. An AJAX request was used in this instance
to minimize the disruption to the User's browsing experience.
 */


function makeNewArticle() {
    document.cookie="edit=newArticle;Secure"
    //this cookie doesn't work for local testing. Please uncomment for deployment
    // document.cookie="edit=newArticle;Secure;HttpOnly"
}

function deleteArticle(articleID) {


    var articleID = articleID;

    if (confirm("Are you sure you want to delete this Article?")) {
        document.cookie = "edit=delete;Secure";

        //This cookie doesn't work for local host testing. Please enable for deployment
        // document.cookie = "edit=delete;Secure;HttpOnly";

        var articleDeletionData = {articleID: articleID};
        $.ajax({
            type: 'post',
            url: "/Articles",
            data: articleDeletionData,
            dataType: "text",
            success: function (resultData) {

                alert("Deletion Complete")
            }
        });

        $('#' + articleID).css("display", "none");


    }


}




