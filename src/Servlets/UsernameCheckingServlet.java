/**
 * created by: Chinchien
 * - for registration. use ajax to get here from login.html
 **/
package Servlets;

import DAOs.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class UsernameCheckingServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.addIntHeader("X-XSS-Protection", 1);
        resp.addHeader("X-Content-Type-Options", " nosniff");
        resp.addHeader("X-Frame-Options", "deny");

        String inputUsername = req.getParameter("inputUsername");
        try (UserDAO userDAO = new UserDAO()) {
            if (userDAO.isTaken(inputUsername)) {
                PrintWriter out = resp.getWriter();
                out.print("{\"errorMsg\":\"" + "The username is already taken" + "\"}");
                out.flush();
                out.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
