package Servlets;

import DAOs.ArticleDAO;
import DAOs.CommentDAO;
import DAOs.User;
import DAOs.UserDAO;
import SecurityPack.AuthenticationModule;
import org.owasp.encoder.Encode;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class UserInfoServlet extends HttpServlet {

//this servlet handles account deletion. This business is a little extended, so we used a separate servlet.

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.addIntHeader("X-XSS-Protection", 1);
        resp.addHeader("X-Content-Type-Options", " nosniff");
        resp.addHeader("X-Frame-Options", "deny");


        Cookie[] cookies = req.getCookies();

        for (int i = 0; i < cookies.length; i++) {

            String value = cookies[i].getValue();
            String userName=(String)req.getSession().getAttribute("username");


             if (value.equals("delete")){


                try (UserDAO delete=new UserDAO()){

                    User toCheck=delete.getAllUserInfoByName(userName);

                    AuthenticationModule toAuthenticate= new AuthenticationModule();
                  boolean safeToDelete= toAuthenticate.check(req.getSession(), toCheck);

                  if (safeToDelete) {

                     try (CommentDAO deleteAllComments=new CommentDAO()){
                         deleteAllComments.deleteAllComments(userName);

                     }

                      try (ArticleDAO deleteAllArticles=new ArticleDAO()){
                          deleteAllArticles.deleteAllArticles(userName);
                      }


                      delete.deleteUser(userName);

                      RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/mainPage.jsp");
                      dispatcher.forward(req, resp);
                  }
                  else{
                      RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/noPermission.jsp");
                      dispatcher.forward(req, resp);
                  }



                }catch (Exception e){
                    System.out.println("An exception occured");
                }

            }



        }





    }
}
