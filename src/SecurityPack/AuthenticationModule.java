package SecurityPack;

import DAOs.Article;
import DAOs.ArticleDAO;
import DAOs.Comment;
import DAOs.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

public class AuthenticationModule {

    public AuthenticationModule() {
    }


/*
This is a class used to add additional authentication to our application. The 'check' method
determines whether the user has the permissions to untertake the business -modifying or deleting- that they are
trying to undertake by checking to see whether the username property stored in the session is the same as the username
property in the object. If it is, the user receives permission to undertake their business.

 */

    public boolean check(HttpSession session, Object tocheck) {

        //toCheck is the object that the user is trying to alter.

        String username = (String) session.getAttribute("username");
        boolean isVerified = false;

        if (tocheck instanceof DAOs.Article) {
            //if toCheck is an article, we check the username property against the session property
            String articleUsername = ((Article) tocheck).getUsername();

            if (articleUsername.equals(username)) {
                isVerified = true;
            }

        } else if (tocheck instanceof User) {
            //if toCheck is a User, we check the username property against the session property
            String userUserName = ((User) tocheck).getUsername();


            if (userUserName.equals(username)) {
                isVerified = true;
            }

        } else if (tocheck instanceof Comment) {
            //if toCheck is a Comment, we check the username property against the session property
            //getting the person who made the comment
            String commentWriterName = ((Comment) tocheck).getUsername();
            int articleId = ((Comment) tocheck).getArticleId();

            String articleWriter = "";

            try (ArticleDAO toAuthenticate = new ArticleDAO()) {

                System.out.println("I got the article Dao");

                String articleIDString = String.valueOf(articleId);

                Article toCheckAgainst = toAuthenticate.getArticleByID(articleIDString);

                //getting the writer of the article containing the comment
                articleWriter = toCheckAgainst.getUsername();

            } catch (Exception e) {
                System.out.println("An exception occurred");
            }

            //if the user wrote the comment OR wrote the article, he/she has permission to delete.
            if (commentWriterName.equals(username) || articleWriter.equals(username)) {
                isVerified = true;
            }

        }


        return isVerified;
    }


}
